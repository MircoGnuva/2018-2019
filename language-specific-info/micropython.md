# Disclaimer

Questo file si pone come un contenitore del percorso parallelo che io (ed eventualmente altri) svilupperò durante questo corso. 
L'idea è di creare un mapping tra i concetti visti a lezione/laboratorio verso il "mondo" MicroPython.

Il presente testo non deve essere considerato come una bibbia (ci mancherebbe), piuttosto una documentazione in evoluzione che sicuramente conterrà diversi errori e per questo motivo subirà svariati aggiornamenti.



# Introduzione

### Cos'è MicroPython

MicroPython, come facilmente deducibile dal nome, è un porting del linguaggio di programmazione Python nel mondo dei micro-controllori e quindi dei sistemi embedded. 
Eredita la filosofia e la sintassi di Python 3; perciò si basa su una sintassi nella maggior parte dei casi molto semplice ed un livello di astrazione piuttosto elevato. 

### Come funziona

Solitamente le schede che supportano questo linguaggio nel cuore hanno un ESP32, tuttavia per ogni scheda è necessario flashare un firmware apposito.

All'accensione, il bootloader carica il file "boot.py" che può essere modificato a piacere; ad esempio può contenere l'intera logica di funzionamento, oppure può essere utilizzato come chiamante di un altro file .py presente in memoria.

### Punti deboli

Non supporta le librerie standard di Python, ma ha una propria lista disponibile a: [qui](<https://github.com/micropython/micropython-lib>)

