# ESP32 board info

Informazioni e link utili per la board NodeMCU-ESP32-S ([Amazon](https://www.amazon.it/dp/B074RG86SR/ref=cm_sw_em_r_mt_dp_U_J-bJCbZ52V2M3))
![](https://components101.com/sites/default/files/components/ESP32-DevKitC.jpg)

  >La nostra board dovrebbe essere una NodeMCU ESP32-S; sembra però essere molto simile e compatibile a livello di pin a quella raffigurata che è una ESP-32-DevKitC V4

## BASIC (*Arduino IDE / Arduino Core per esp32*)
  - Installazione/utilizzo con Arduino IDE Boards Manager [[github](https://github.com/espressif/arduino-esp32/blob/master/docs/arduino-ide/boards_manager.md)]
  - API per esp8266 [[github](https://github.com/esp8266/Arduino/tree/master/doc)] [[readthedocs](https://arduino-esp8266.readthedocs.io/en/latest/index.html)]
  - Esempi per esp32 (visibili anche dall'Arduino IDE) [[github](https://github.com/espressif/arduino-esp32/tree/master/libraries)]

  > NB: Attualmente sembrano NON esserci differenze tra le API di esp8266 e esp32. In base a questo [issue](https://github.com/espressif/arduino-esp32/issues/1363#issuecomment-385190520) la documentazione standard di Arduino deve essere integrata con le API spedifiche di Arduino Core per esp32. Non sembra esserci documentazione chiara e spedifica per queste API ma pittosto bisogna, di volta in volta, andare a guardare [gli esempi](https://github.com/espressif/arduino-esp32/tree/master/libraries) dentro le singole librerie che si vogliono utilizzare.


## ADVANCED (*Other*)
  - Installazione/utilizzo con PlatformIO [[github](https://github.com/espressif/arduino-esp32/blob/master/docs/platformio.md)]
  - Compilazione sorgenti con makefile (e anche flashing credo...) [[github](https://github.com/plerup/makeEspArduino)]
  - Flashing firmware OTA (molto interessante per IoT) [[github](https://github.com/espressif/arduino-esp32/blob/master/docs/OTAWebUpdate/OTAWebUpdate.md)] (atrent: c'è anche OTA non via web ma simil-ftp)
  - Esptool utility (utility per comunicare e interagire con la memoria flash) [[github](https://github.com/espressif/esptool)]

  > NB: se lo provate NON scrivete a caso e soprattutto non sovrascrivete il bootloader che poi diventa complicato ripristinarlo

## HARDWARE
  - Pinout e Device Summary NodeMCU ESP32-S (teoricamente la nostra board) [[zerynth](https://docs.zerynth.com/latest/official/board.zerynth.nodemcu_esp32/docs/index.html)]
  - Pinout e info generali ESP32-DevKitC V4 (info più verbose del link precedente) [[components101](https://components101.com/microcontrollers/esp32-devkitc)]
  - Schematics ESP32 [[shenzhen2u](https://www.shenzhen2u.com/NodeMCU-32S)]

## DRIVER
  - Driver USB seriale Win/MacOS/Linux [[silabs](https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers)]
  - WiFi eduroam per ESP32 [[github](https://github.com/martinius96/ESP32-eduroam)]
