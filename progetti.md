# Progetti d'esame

Pensate a progetti che privilegino l'hardware (sensori e attuatori, interfacciamento) rispetto al software: cose come app, siti web, altri sistemi "grossi" devono essere solo corollari (e sono /out of scope/ rispetto al corso). Mi interessa che vi "scontriate" col mondo fisico.
Altra cosa importante (che dobbiamo ancora vedere a lezione): l'uso di sistemi /closed-loop/ cioè con un minimo di feedback.


## Modalità

Gruppi sono possibili, ma limitatamente a due studenti (oltre si rischia dispersione delle competenze).

Ogni studente o gruppo deve dichiararsi su questa pagina (seguire il template)

	Nome progetto

	Autori: nome1 cognome1, nome2 cognome2

	Descrizione: breve descrizione (hw/sw) del progetto

	FIXME (repo qui) Link a repo: creare un repository git (su un server tipo github o gitlab o altro pubblico) e linkarlo qui

	Licenza scelta: (per la scelta consultare ad es. https://www.gnu.org/licenses/licenses.html)


## Idee

* Monopattino elettrico con recupero energia in frenata
* Sensori ambientali non banali: combinare più sensori "banali" per misurare una proprietà di un ambiente. Es. localizzazione indoor di un device, contare persone in un ambiente, ...
* Inseguitore solare (me lo ha proposto uno di voi, segnalo qui per dare idee ulteriori)
* Integrazione di sensori in sistemi domotici, spiego: oggi esistono molti sistemi (anche liberi, cfr. openHAB e HomeAssistant) di gestione domotica, tali sistemi "parlano" già molti protocolli standard (MQTT, etc.) e sono capaci di interfacciarsi con sensori "standard". Un buon esercizio potrebbe essere quello di realizzare un sensore e renderlo "integrabile" in questi sistemi, cioè dotarlo della capacità di parlare facilmente con tali sistemi, in modo da non dover costringere gli utilizzatori a scrivere ulteriore software, ma solo scrivere un file di conifgurazione.
* Installazioni (anche interattive) "artistiche": animazioni d'acqua (https://www.youtube.com/watch?v=gusJeslMbLc), luce (https://www.youtube.com/watch?v=KmjP5VwuqBw dal minuto 35 circa, ma anche tutto), suono, etc.
* ...

## In itinere (progetti iniziati, ancora da presentare all'esame)

...


## Presentati

### SimSim

Nome progetto: SimSim

Autore: Andrei Ciulpan

Descrizione: 

   Sistema di controllo accessi basato su Arduino. 
   Il sistema funziona con diverse modalità di riconoscimento tra cui abbiamo:
      
     - RFID 
     - telecomando (con RF receiver) 
     - keypad per poter accedere via password
    
   Il sistema manda i log di accessi tramite una richiesta HTTP ad un database server in locale 
   (ho creato anche la parte di back-end, ma non sta sul repo) con un ESP-01
   Il sistema dispone inoltre di un display LCD 16x2 
     
Repository: https://github.com/Jolsty/SimSim

Documentazione su https://github.com/Jolsty/SimSim/wiki * WORK IN PROGRESS *

Licenza scelta: CC BY-ND
  



### Salvaduino

Autori: D. Bellisario

Descrizione: obiettivo del progetto è la realizzazione di un salvadanaio controllato da Arduino.
Il salvadanaio:
    * accetta monete da 0.50, 1 e 2 EUR;
    * mostra importo e numero di monete su un display 16 x 2 LCD;
    * salva i dati su EEPROM per mantenere le informazioni anche in caso di spegnimento;
    * autorizza il prelievo solo in possesso di un token RFID;

(altre informazioni sul wiki [[https://github.com/DB375237/salvaduino/wiki]] dedicato)


Repository: https://github.com/DB375237/salvaduino

Licenza scelta: CC0 1.0 Universal.



### RadioArduino

Autori: Adriano Cofrancesco

Descrizione: RadioArduino è un progetto che ha lo scopo di emulare una Radio FM. In particolare questa radio può essere pilotata manualmente, con l'uso di un potenziometro per il cambio frequenza, oppure attraverso appositi messaggi via WiFi utilizzando il protocollo MQTT. Basandosi sullo schema publish/subscribe con MQTT RadioArduino può operare nella modalità  Subscriber, in tal caso può solo ricevere messaggi e rispondere al Publisher in maniera automatica a specifiche richieste, oppure come Publisher, in questo caso oltre alle funzionalità del subscriber può cambiare la frequenza a tutti i subscribers, verificare su quale frequenza sono sintonizzati e anche richiederne l'identificativo. Ogni subscriber apparterrà ad un gruppo, inizialmente di default, che potrà essere cambiato in autonomia così da ricevere messaggi da publisher diversi.

Hardware:
  * ESP8266 NodeMCU
  * LCD 16x2
  * 2x potenziometri 10K
  * TEA5767 FM Radio module

Link a repo: https://github.com/adrianocofrancesco/RadioArduino

Licenza scelta: GPLv3



### RemoteControlledGreenhouse

Autore: Marco De Nicolo

Descrizione: Ho realizzato una serra che può essere controllata da qualsiasi dispositivo, connesso alla stessa rete di questa, attraverso il protocollo OSC.
In particolare si può utilizzare un server per gestire, attraverso un'interfaccia grafica, una o più serre e renderle accessibili anche all'esterno della propria rete.
Per alimentare la serra ho utilizzato un power supply di un vecchio fisso, così da poter utilizzare tutti i diversi voltaggi che mi servivano.
La serra è automatizzata, quindi se l'igrometro segna un'umidità del terreno troppo bassa si apre l'elettrovalvola per innaffiare, se la temperatura o l'umidità è troppo alta si azionano le ventole e così via (i livelli minimi e massimi si possono settare).
La serra comunica con un server per settare l'ora e permettere all'utente di aggiungere innaffiature programmate.
La luce può essere automatica, quindi si accende con il buio, oppure manuale.
HW utilizzato:
  * 2 Ventole
  * elettrovalvola
  * led rosso (si può utilizzare una lampada adatta)
  * igrometro
  * sensore umidità e temperatura DHT11
  * sensore livello acqua
  * fotoresistenza
  * relè 4 canali
  * esp8266
  * power supply
  * breadboard, cavi jumper e altri componenti non elettronici

Link a repo: https://github.com/Maerk/RemoteControlledGreenhouse , https://github.com/Maerk/RemoteControlledGreenhouseServer

Licenza scelta: MPL 2.0







### SmartGarden

Autori: Alessandro Gigliotti, Giovanni Reni

Descrizione: in questo progetto, abbiamo realizzato un sistema di gestione intelligente di un piccolo orto o giardino. È presente un impianto d'irrigazione, basato sul controllo di ciò che accade e quindi di come si modifica, l'ambiente circostante. L'impianto entra infatti autonomamente in azione, quando si verificano determinate condizioni (come secchezza del terreno o assenza di pioggia), tenendo costantemente sotto controllo varie informazioni sull'aria, terra, meteo e sullo stato degli strumenti d'irrigazioni utilizzati. L'irrigazione può entrare in funzione, utilizzando una cisterna che raccoglie l'acqua piovana, oppure tramite un impianto idraulico. È altresì possibile attivare l'irrigazione anche manualmente da un utente tramite un pulsante fisico. Le informazioni rilevate tramite i sensori in giardino, vengono inviate e visualizzate su un display, attaccato ad un'altra board, posizionato in un luogo chiuso (ad esempio in casa). Affianco al display, ci saranno anche 4 led, che segnalano la presenza/assenza di pioggia, la presenza/assenza di sole, lo stato del serbatoio pieno o vuoto. Ci sono infine 3 pulsanti fisici, posizionati accanto al display, per azionare manualmente l'irrigazione, per accendere una lampada per illuminare e un tasto per arrestare entrambe queste attività. Inoltre il sistema sfrutta un'applicazione mobile, dalla quale sono consultabili tutte le informazioni lette dai sensori e lo stato della cisterna. Sono presenti inoltre 3 pulsanti virtuali, gemelli di quelli fisici, per azionare gli strumenti del progetto da un dispositivo. Infine l'app invia anche notifiche, relative agli eventi che accadono nel sistema.

Link a repo: https://github.com/GioReni/SmartGarden

Licenza scelta: GPLv3

### The Mouse

Autori: Simone Calcaterra, Matteo Negri

Descrizione: L'idea del progetto è quella di creare un mouse robotizzato, capace di percorrere tragitti prestabiliti secondo misure date.

per fare ciò utilizziamo la tecnologia del mouse per avere un riscontro sulla tratta percorsa in modo tale da compiere tragitti più o meno precisi.

Il software da noi utilizzato è una libreria, la PS/2 mouse presente in internet.
L'hardware da noi utilizzato è:
  * Un mouse PS\2 
  * Una board Arduino pro Micro con integrato ATMEGA32U
  * Due motori DC da 3.3 V

Il nostro progetto focalizza molto l'attenzione sulla libreria e sul protocollo PS\2.

Link a repo: https://github.com/mnegri/TheMouse

Licenza scelta: GPLv3



### Macchina "intelligente"

Autori: Magni Andrea & Mercanti Davide

Descrizione: Il progetto che abbiamo deciso di realizzare prevede la costruzione di una "macchina intelligente". La macchina che costruiremo andrà a svolgere svolgere diverse funzioni di valutazione e risoluzione di problematiche.
  * La macchina dovrà riuscire a muoversi in una strada composta da due corsie rimanendo all'interno di una corsia grazie alla presenza di quattro sensori ad infrarossi posizionati nella parte frontale. 
  * La presenza di un semaforo per gestire il traffico gestito da una board differente rispetto a quella della macchina. Entrambe le board avranno a disposizione un modulo radio per consentire la comunicazione.  
  * Nel caso in cui la macchina si stia avvicinando ad un semaforo valuti la sua condizione e regoli la velocità di conseguenza. 
  * La presenza di possibili ostacoli durante il percorso, i quali verranno individuati da un modulo a ultrasuoni nella parte frontale della macchina.
  * Nel caso in cui la macchina trovi un ostacolo durante il percorso, i quali possono essere considerati fissi (una macchina in panne, lavori in corso) o mobili (dei pedoni che attraversano la strada) valuti il tipo di ostacolo e decida se continuare il percorso o effettuare una manovra di cambio corsia per poi continuare il percorso.

Link a repo: https://github.com/AndreaMagni/SistemiEmbedded 

Licenza scelta: GPLv3





### SHARON-Macchina Self Tuning & Path Follower

Autori: Simone Scaravati, Noah Rosa, Stefano Radaelli 

Descrizione: Sharon è una macchina controllabile tramite WiFi, grazie all'uso del protocollo OSC. 
È in grado di bilanciare automaticamente la potenza dei suoi motori, in modo da poter andare dritta senza la necessità di andare a regolare manualmente la direzione da lei seguita(errore che capita spesso a causa dell'imprecisione dei motori).
Inoltre si interfaccia con un software (che sarà probabilmente disponibile per tutti i s.o.), scritto su Processing, che permetterà di disegnare graficamente un percorso, il quale sarà seguito dalla macchina il più fedelmente possibile.

Ai fini di testing, il progetto si appoggia all'app AndrOSC (disponibile gratuitamente su Play Store), per guidare manualmente la macchina e per farla anche muovere autonomamente grazie ad un sensore di distanza posto nella parte anteriore della carrozzeria. 
Per poter utilizzare l'app facilmente mettiamo a disposizione un preset creato da noi, da collocare in un path specifico descritto nel readme.

Link a repo: https://github.com/simoneScaravati/Sharon-Path-Finder

Licenza scelta: GPLv3

### ESP8266 print server

Autore: Gianluca Nitti

Descrizione: print server basato su ESP8266 per aggiungere connettività di rete a stampanti con porta parallela o seriale. Le stampanti con porta parallela possono essere collegate direttamente (occupando 10 GPIO) oppure tramite shift register (usando "solo" 5 pin di I/O). Sono supportati i protocolli di rete [[http://lprng.sourceforge.net/LPRng-Reference-Multipart/appsocket.htm|AppSocket]] ed [[https://en.wikipedia.org/wiki/Internet_Printing_Protocol|IPP]]. Presente anche una coda di stampa sulla memoria flash a bordo dell'ESP8266 per poter accettare altri job anche quando la stampante è occupata, ma attualmente funziona solo se il job di stampa in attesa rientra completamente nella memoria (quando la stampante si libera, prima finisce di ricevere il nuovo job e poi inizia a stamparlo; migliorabile perchè potrebbe iniziare non appena la stampante è libera, facendo quindi spazio sulla flash).

Link a repo: https://github.com/gianluca-nitti/printserver-esp8266/ (il branch develop viene aggiornato più spesso, facendo il merge in master solo quando è completato lo sviluppo di una funzionalità significativa)

Licenza scelta: GPLv3

### DAMS_01 Sintetizzatore digitale

Autore: Marco Colussi

Descrizione: Il DAMS_01 è un sintetizzatore digitale sviluppato in PureData che utilizza dei sensori ambientali per creare una sintesi diversa e unica in ogni luogo in cui viene usato. Dal lato hw vediamo l'impiego di un raspberry PI3 per far girare il software di PureData, un Arduino UNO per la gestione degli input e la comunicazione con il sintetizzatore via firmata_extended, codice Arduino che modifica il già esistente standardFirmata per permettere di falsare letture e la quantità di pin analogici presenti sulla board.
I sensori utilizzati sono:
  - RGB Led;
  - ADXL 345;
  - Touch sensor;
  - LDR;
  - DHT11;
  - Analog Thumb Joystic;
  - Multiplexer CD4067BE;
  - 5x potenziometri 10kΩ;
  - 5x bottoni;


Link a repo: https://github.com/warpcut/DAMS_01 

Licenza scelta: GPLv3
