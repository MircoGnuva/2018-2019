# Diario lezioni

Nota bene: a lezione non è detto si riesca ad affrontare tutti gli argomenti, ma per l'esame vanno conosciuti i capitoli del libro elencati nel README.

## Lezioni (in ordine cronologico inverso)

### Future

* 2019-05-10, Intervento di ST
* ...
* 2019-04-29, NON c'è lezione
* 2019-04-26, NON c'è lezione
* 2019-04-22, NON c'è lezione
* ...
* 2019-04-08, Intervento di BOSCH
* ...
* 2019-xx-xx, (lab) tipi di dato e memoria
* 2019-04-01, forme d'onda, PWM
* 2019-03-29, (lab) I/O input (anche PULLUP) + analogici


### Passate

* 2019-03-25, fine componenti (compreso transistor e c.i.), strumenti di misura, cenni sui sensori
* 2019-03-22, (lab) I/O inizio, collegamento componentistica
* 2019-03-18, componenti, specie condensatori, RC
* 2019-03-15, (lab) Arduino IDE
* 2019-03-11, richiamo sui principi, leggi di Ohm e Kirchhoff, resistenze, partitore, serie e parallelo di resistenze, interruttori, lampadine, relé
* 2019-03-04, intro Sistemi Embedded, architetture, monoprog e multiprog, MdT, memoria
* 2019-03-01, intro corso, panoramica Sistemi Embedded


## Argomenti da evadere (non in ordine di presentazione)

FIXME verificare ci siano tutti

* Conversione AD e DA
* Multiplexing
* Controlli automatici (PID)
* Modo di pensare
* Real Time
* Licenze Software
* Forme d’onda
* PWM (Pulse Width Modulation)
* Montaggio ﬁsico
* Sensori
* Attuatori
* MEMS
* Architetture Embedded (panoramica)
* Memorie, I/O e comunicazione
* I sistemi GNU/Linux (panoramica)
* FreeRTOS (cenni, forse)
* Arduino e Wiring
* Rete e protocolli (panoramica)
* Firmata
* OSC
* Motori, ponte H, etc.
* I2C
* TaskScheduler
* MQTT
